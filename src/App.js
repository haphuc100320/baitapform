import logo from './logo.svg';
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import './App.css';
import Ex_game_xuc_xac from './Ex_Game_Xuc_Xac/Ex_game_xuc_xac';
import Demo_LifeCycle from './Demo_LifeCyle/Demo_LifeCycle';
import Ex_QuanLyNguoiDung from './Ex_QuanLyNguoiDung/Ex_QuanLyNguoiDung';

function App() {
  return (
    <div className="App">
     {/* <Ex_game_xuc_xac/> */}
     {/* <Demo_LifeCycle/> */}
     <Ex_QuanLyNguoiDung/>
     
    </div>
  );
}

export default App;
