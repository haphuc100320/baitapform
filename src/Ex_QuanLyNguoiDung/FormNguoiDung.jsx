import React, { Component } from 'react'
import { initaiUser } from './ultis_ex_QuanLyNguoiDung'
/**
 * hoTen
 * taiKhoan
 * matKhau
 * email
 */
export default class FormNguoiDung extends Component {
    // state của form người dùng
    state={
        user: initaiUser
    }

    // xử lys sự kiện 
    handleChangeUser=(event) => { 
        // console.log(event.target.value);
        // console.log(event.target.name);

        let value= event.target.value
        let key= event.target.name
        // tạo 1 biến cloneUser rồi thêm vào 
        let cloneUser={...this.state.user, [key]:value};
        // cloneUser=value
        this.setState({user:cloneUser})
     }
     
    

    render() {
        console.log(this.state.user)
        return (
            <div>
                <form >
                    <div className="form-group">
                        <label htmlFor />
                        <input onChange={(e) => { 
                            this.handleChangeUser(e)
                         }} value={this.state.user.hoTen} type="text"
                            className="form-control" name="hoTen" placeholder="Tên" />
                    </div>

                    <div className="form-group">
                        <label htmlFor />
                        <input onChange={(e) => { 
                            this.handleChangeUser(e)
                         }} value={this.state.user.taiKhoan} type="text"
                            className="form-control" name="taiKhoan" placeholder="taiKhoan" />
                    </div>

                    <div className="form-group">
                        <label htmlFor />
                        <input onChange={(e) => { 
                            this.handleChangeUser(e)
                         }} value={this.state.user.matKhau} type="text"
                            className="form-control" name="matKhau" placeholder="matKhau" />
                    </div>

                    <div className="form-group">
                        <label htmlFor />
                        <input onChange={(e) => { 
                            this.handleChangeUser(e)
                         }} value={this.state.user.email} type="text"
                            className="form-control" name="email" placeholder="email" />
                    </div>

                    <button type='button' onClick={() => { 
                        this.props.handleThemNguoiDung(this.state.user)
                     }} className='btn btn-primary'>Thêm Người Dùng</button>
                </form>
            </div>
        )
    }
}
