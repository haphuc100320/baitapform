import React, { Component } from 'react'
import DanhSachNguoiDung from './DanhSachNguoiDung'
import FormNguoiDung from './FormNguoiDung'

export default class Ex_QuanLyNguoiDung extends Component {
    // danhSachNguoiDung object rỗng
    state={
        danhSachNguoiDung:[],
    }
    // hàm thêm người dùng
    handleThemNguoiDung=(user) => { 
        this.setState({
            danhSachNguoiDung:{...this.setState.danhSachNguoiDung,user}
        })
       }
    
  render() {
    return (
      <div className='container'>
        <FormNguoiDung handleThemNguoiDung={this.handleThemNguoiDung}/>
        
        <DanhSachNguoiDung danhSachNguoiDung={this.state.danhSachNguoiDung}/>
      </div>
    )
  }
}
