import React, { Component } from 'react'

export default class DanhSachNguoiDung extends Component {
    renderDanhSachNguoiDung= () => { 
        return this.props.danhSachNguoiDung.map((item, index) => {
            let { hoTen, taiKhoan, matKhau, email, } = item;
            return (
              <tr >
                
                <td>{hoTen}</td>
                <td>{taiKhoan}</td>
                <td>{matKhau}</td>
                <td>{email}</td>
            </tr>
        )
       })
     }
  render() {
    console.log(this.props.danhSachNguoiDung)
    return (
      <div>
        <table className='table mt-5'>
            <thead>
                <th>Tên</th>
                <th>Tài Khoản</th>
                <th>Mật Khẩu</th>
                <th>Email</th>
            </thead>
            <tbody>
                {/* render danh sách người dùng */}
                {this.renderDanhSachNguoiDung()}
            </tbody>
        </table>
      </div>
    )
  }
}
