import React, { Component } from 'react'
import { connect } from 'react-redux'

let styles = {
    btnGame:{
        fontSize:"40px",
        width:"150",
        height:"150"
    }
}

class XucXac extends Component {
  render() {
    console.log(this.props)
    return (
      <div style={styles.btnGame} className='d-flex'>
        <button style={styles.btnGame} className='text-white btn btn-primary'>Tài</button>
        <button style={styles.btnGame} className='text-white btn btn-danger'>Xỉu</button>
      </div>
    )
  }
}

let mapStateToMap=(state) => { 
    return {
        mangXucXac: state.xucXacReducer.mangXucXac
    }
 }

 export default connect (mapStateToMap)(XucXac)